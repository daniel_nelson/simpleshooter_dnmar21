﻿using UnityEngine;
using System.Collections;

//This script manages the player object
public class Player : Spaceship
{	
	public float playerDelay = 0.1f;
	public string fireKey = "space";
	private int chargeCount;
	public int chargeInterval;
	private bool isShooting;
	void Update ()
	{
		//Get our raw inputs
		float x = Input.GetAxisRaw ("Horizontal");
		float y = Input.GetAxisRaw ("Vertical");
		//Normalize the inputs
		Vector2 direction = new Vector2 (x, y).normalized;
		//Move the player
		Move (direction);
		shot ();
	}
	
	void Move (Vector2 direction)
	{
		//Find the screen limits to the player's movement
		Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		//Get the player's current position
		Vector2 pos = transform.position;
		//Calculate the proposed position
		pos += direction  * speed * Time.deltaTime;
		//Update the player's position
		pos.x = Mathf.Min(Mathf.Max(min.x, pos.x), max.x);
		pos.y = Mathf.Min(Mathf.Max(min.y, pos.y), max.y);
		transform.position = pos;
	}

	void OnTriggerEnter2D (Collider2D c)
	{
		//Get the layer of the collided object
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		//If the player hit an enemy bullet or ship...
		if( layerName == "Bullet (Enemy)" || layerName == "Enemy")
		{
			//...and the object was a bullet...
			if(layerName == "Bullet (Enemy)" )
				//...return the bullet to the pool...
			    ObjectPool.current.PoolObject(c.gameObject) ;
			//...otherwise...
			else
				//...deactivate the enemy ship
				c.gameObject.SetActive(false);

			//Tell the manager that we crashed
			Manager.current.GameOver();
			//Trigger an explosion
			Explode();
			//Deactivate the player
			gameObject.SetActive(false);
		}
	}
	protected override void OnEnable ()
	{

	}
	public IEnumerator Shoot (int volleyCount)
	{
		isShooting = true;
		//Loop indefinitely
		for(int i = 0; i < volleyCount; i++) 
		{
			fireShot (volleyCount);
			//Wait for it to be time to fire another shot
			yield return new WaitForSeconds (playerDelay);
		}
		yield return new WaitForSeconds (playerDelay*3);
		isShooting = false;
	}
	void shot()
	{
		if (Input.GetKey (fireKey) && !isShooting) {
			chargeCount++;
		}
		if (Input.GetKeyUp (fireKey) && !isShooting) {
			int volleyCount = 2;
			if (chargeCount > chargeInterval) {
				volleyCount = 4;
			}
			if (chargeCount > 2 * chargeInterval) {
				volleyCount = 8;
			}
			chargeCount = 0;
			StartCoroutine ("Shoot", volleyCount);
		}
	}
	void fireShot(int volleyCount)
	{
		if (volleyCount % 2 != 0)
			volleyCount++;
		volleyCount = Mathf.Min (shotPositions.Length, volleyCount);
		//If there is an acompanying audio, play it
		if (GetComponent<AudioSource> ())
			GetComponent<AudioSource> ().Play ();
		//Loop through the fire points
		for (int i = 0; i < volleyCount; i++) {
			//Get a pooled bullet
			GameObject obj = ObjectPool.current.GetObject (bullet);
			//Set its position and rotation
			obj.transform.position = shotPositions [i].position;
			obj.transform.rotation = shotPositions [i].rotation;
			//Activate it
			obj.SetActive (true);
		}
	}
}